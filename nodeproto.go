package nodeproto

import (
	"fmt"
	"io"
	"log"
	"time"

	"github.com/sigurn/crc16"
	"github.com/tarm/serial"
)

// PktType Type of packet
type PktType byte

// PktStatus - Recv state of the packet
type PktStatus int

// RecvStatus - General receive state
type RecvStatus int

// SendStatus - TX state
type SendStatus int

// RxState - RX state
type RxState int

const (
	// PktTypeSingle - Single target packet
	PktTypeSingle PktType = 0
	// PktTypeMulticast - Multicast packet
	PktTypeMulticast PktType = 40
	// PktTypeAck - ACK packet
	PktTypeAck PktType = 128
	// PktTypeNack - ACK packet
	PktTypeNack PktType = 192
)

/*
	RecvIdle - Idle
	RecvBusy - Busy
	RecvDataReady - Packet is received and the data is verified
	RecvAck - Acknowledged
	RecvNack - Not Acknowledged
	RecvError - Error
*/
const (
	RecvIdle RecvStatus = 1 << iota
	RecvBusy
	RecvDataReady
	RecvError
	RecvTimeout
)

/*
	SendOk - Ok
	SendCollision - There was a collision when sending
	SendNoAck - No ACK was received
	SendAck - ACK was received
	SendRxBusy - RX is busy so can't send
	SendTimeout - Timed out while sending
*/
const (
	SendOk SendStatus = 1 << iota
	SendCollision
	SendNoAck
	SendRxBusy
	SendTimeout
)

/*
	Different phases when receiving data.

	RxIdle - Idle and ready.
	RxIgnore - Ignore
	RxHDest - Got pkt destination
	RxHSrc - Got pkt src
	RxRcvData - Got data
	RxVerify - Verifying pkt
*/
const (
	RxIdle RxState = 1 << iota
	RxIgnore
	RxHDest
	RxHSrc
	RxRcvData
	RxVerify
)

const (
	startByte = 0xA0
	escByte   = 0xE0
	stopByte  = 0x80
	escMask   = 0xFF
)

/*
	PktStatusOk - Ok
*/
const (
	PktStatusOk PktStatus = 1 << iota
	PktStatusBadCrc
	PktStatusOverflow
)

// RS485Packet - Info about the packet
type RS485Packet struct {
	Type   PktType
	SrcID  int
	Length int
	Buffer []byte
	State  PktStatus
}

// NodeController - The controller struct
type NodeController struct {
	NodeID byte
	Packet RS485Packet

	logPktInfo           bool
	NoCollisionDetection bool
	IdleTimeout          time.Duration

	// State vars
	crcTable *crc16.Table
	ctrl     GPIOCtrl
	s        *serial.Port

	rxEscState     bool
	rxCrc          uint16
	rxState        RxState
	rxLastReceived time.Time
}

func (pkt *RS485Packet) GetData() []byte {
	return pkt.Buffer[2 : len(pkt.Buffer)-2]
}

// NewController - Create a new NodeController
func NewController(serialPort *serial.Port, ctrl GPIOCtrl) *NodeController {
	return &NodeController{
		NodeID: 1 & 63,

		crcTable: crc16.MakeTable(crc16.CRC16_KERMIT),
		ctrl:     ctrl,
		s:        serialPort,

		logPktInfo:           true,
		NoCollisionDetection: false,
		IdleTimeout:          time.Duration(time.Millisecond * 300),

		rxCrc:      0,
		rxEscState: false,
		rxState:    RxIdle,
	}
}

func (nc *NodeController) logPkt(info string) {
	if nc.logPktInfo {
		log.Printf("Packet %s", info)
	}
}

// Receive - get a packet
func (nc *NodeController) Receive(timeout int) (Recv RecvStatus) {
	var status RecvStatus

	for {
		log.Println("Trying to receive...")
		status = nc.recvPacket(timeout)

		// Return if IDLE, BUSY or TIMEOUT. In sync mode, only TIMEOUT can happen.
		if status < RecvDataReady {
			return status
		}

		// This means we have received a packet for us. If this is an ACK or NACK, we should ignore it because "send" is responsible for this.
		// NOTE: This is even done on ERROR packets, as we cannot NACK a received ACK or NACK anyway.
		if nc.Packet.Type >= PktTypeAck {
			continue
		}

		// Check for single addressed packet, which should be acked or nacked.
		if nc.Packet.Type == PktTypeSingle {
			if status == RecvError {
				nc.logPkt("RX: nack")
			} else {
				nc.logPkt("RX: ack")
			}

			var responseStatus PktType

			// This may result in a collision, but ACKs are fire and forget. /* SendResult ackRes = */
			if status == RecvError {
				responseStatus = PktTypeNack
			} else {
				responseStatus = PktTypeAck
			}

			nc.logPkt(fmt.Sprintf("Sending response %d to %d", responseStatus, nc.Packet.SrcID))

			nc.sendRaw(responseStatus, nc.Packet.SrcID, nil, 0)
		}

		if status == RecvDataReady || timeout == 0 {
			break
		}
	}

	log.Println("RX: Received")

	return status
}

func (nc *NodeController) recvPacket(timeout int) RecvStatus {
	//recv := make(chan RecvStatus, 1)
	tmout := time.After(time.Duration(timeout)*time.Millisecond)
/*	go func() {
		recv <- nc.recvUpdate()
	}()
*/
	var status RecvStatus

	for {
		select {
		/*case status = <-recv:
			if timeout == 0 {
				return status
			}

			if status > RecvBusy {
				goto END
			}
		case <-time.After(time.Millisecond * time.Duration(timeout)):
			log.Println("recvPacket: timeout")
			goto END
		}*/
		default:
		status = nc.recvUpdate()
			if timeout == 0 {
				return status
			}

			if status > RecvBusy {
				goto END
			}
		case <-tmout:
		log.Println("recvPacket: timeout")
			goto END
		}
	}

END:
	log.Println("recvPacket: end")
	return status
}

// Update - Reads the next packet
func (nc *NodeController) recvUpdate() RecvStatus {
	buffer := make([]byte, 1)
	recvState := RecvIdle

	for {
		_, err := nc.s.Read(buffer)

		if err != nil && err != io.EOF {
			recvState = RecvError
			goto cleanExit
		}

		// TODO: Add timeout ?
		if err == io.EOF {
			// Nothing was received, move on?
			break
		}

		if recvState == RecvIdle {
			recvState = RecvBusy
		}

		nc.rxLastReceived = time.Now()

		recv := buffer[0]
		if !nc.rxEscState && recv == stopByte {
			if nc.rxState != RxRcvData {
				nc.rxState = RxIdle
				log.Println("rX: Ei")

				if recvState == RecvError {
					goto cleanExit
				}

				continue
			} else {
				log.Println("rX: Ep")
				nc.rxState = RxVerify
			}
		} else {
			if recv == escByte {
				nc.rxEscState = true
				continue
			}
			if nc.rxEscState {
				recv ^= escMask
				nc.rxEscState = false
			}
		}

		nc.logPkt(fmt.Sprintf("State: '%d'", nc.rxState))
		nc.logPkt(fmt.Sprintf("Data: '%x'", recv))

		id := recv & 63

		switch nc.rxState {
		case RxIdle, RxIgnore:
			if recv != startByte {
				nc.logPkt("ignored")
				break
			}

			nc.logPkt("new")

			nc.Packet = RS485Packet{
				Buffer: make([]byte, 0, 128),
			}

			nc.rxState = RxHDest
			break
		case RxHDest:
			if PktType(recv&192) == PktTypeMulticast {
				if (id & nc.NodeID) != id {
					nc.logPkt(fmt.Sprintf("mcast not destined for us %d %d\n", id, nc.NodeID))

					nc.rxState = RxIgnore
					break
				}
			} else if id != nc.NodeID {
				// Not Multicast, so check if the address is for us
				nc.rxState = RxIgnore
				nc.logPkt(fmt.Sprintf("not destined for us dst: %d %d\n", id, nc.NodeID))
				break
			}

			nc.Packet.Buffer = append(nc.Packet.Buffer, recv)
			nc.Packet.Length = nc.Packet.Length + 1

			nc.Packet.Type = PktType(recv & 192)
			nc.rxState = RxHSrc

			nc.logPkt(fmt.Sprintf("dst %d", id))
			break
		case RxHSrc:
			nc.Packet.SrcID = int(recv & 63)

			nc.Packet.Length = nc.Packet.Length + 1
			nc.Packet.Buffer = append(nc.Packet.Buffer, recv)

			nc.rxState = RxRcvData
			nc.rxCrc = crc16.Update(nc.rxCrc, buffer, nc.crcTable)

			nc.logPkt(fmt.Sprintf("src %d", nc.Packet.SrcID))
			break
		case RxRcvData:
			if nc.Packet.Length >= 128 {
				nc.logPkt("buffer overflow")
				nc.rxState = RxIgnore
				nc.Packet.State = PktStatusOverflow
				recvState = RecvError
				break
			}

			nc.Packet.Length = nc.Packet.Length + 1
			nc.Packet.Buffer = append(nc.Packet.Buffer, recv)

			break
		case RxVerify:
			nc.logPkt("verify")

			asHex := []string{}
			for i := range nc.Packet.Buffer {
				asHex = append(asHex, fmt.Sprintf("%x", nc.Packet.Buffer[i]))
			}

			nc.logPkt(fmt.Sprintf("Data: %s", asHex))

			// Packet reception is complete. Verify CRC.
			rxA := uint16(nc.Packet.Buffer[nc.Packet.Length-3] << 8)
			rxB := uint16(nc.Packet.Buffer[nc.Packet.Length-2])
			rxCrc := rxA | rxB

			calcCrc := uint16(0)
			for i := 0; i < len(nc.Packet.Buffer)-2; i++ {
				calcCrc = crc16.Update(calcCrc, nc.Packet.Buffer[i:i+1], nc.crcTable)
			}

			calcCrc = crc16.Complete(calcCrc, nc.crcTable)

			log.Printf("CRC received: %d calculated: %d", rxCrc, calcCrc>>8)

			if rxCrc == calcCrc>>8 {
				// Send ACK
				nc.Packet.State = PktStatusOk

				nc.logPkt(fmt.Sprintf("is ready - length %d", nc.Packet.Length))
				recvState = RecvDataReady
			} else {
				nc.Packet.State = PktStatusBadCrc
				recvState = RecvError

				nc.logPkt("badcrc")
			}

			goto cleanExit
		}
	}

	// Check if RX state should be reset to IDLE, due to a timeout
cleanExit:
	if nc.rxState != RxIdle && time.Now().Sub(nc.rxLastReceived) > nc.IdleTimeout {
		log.Println("Going to idle")
		nc.rxState = RxIdle
	}

	return recvState
}

func (nc *NodeController) rxBusy() bool {
	return nc.rxState > RxIdle
}

func (nc *NodeController) delayRandom() {
	time.Sleep(10 * time.Nanosecond)
}

/*
	Send a single byte and escape if necassary
*/
func (nc *NodeController) sendEscByteCrc(currentCrc uint16, data byte, noCrc bool) uint16 {
	if !noCrc {
		currentCrc = crc16.Update(currentCrc, []byte{data}, nc.crcTable)
	}

	switch data {
	case startByte:
	case escByte:
	case stopByte:
		nc.s.Write([]byte{data})
		data ^= escByte
	}

	// Write the data byte
	nc.s.Write([]byte{data})

	return currentCrc
}

func (nc *NodeController) sendRaw(pktType PktType, address int, buffer []byte, length int) SendStatus {
	var txByte uint8

	useLoopback := nc.ctrl.SetTransmit(!nc.NoCollisionDetection)

	// Send START byte
	nc.s.Write([]byte{startByte})

	if useLoopback {
		nc.s.Flush()

		var recv = make([]byte, 1)
		nc.s.Read(recv)

		if recv[0] != startByte {
			fmt.Printf("%x\n", recv[0])
			log.Println("Colission detected.")
			nc.ctrl.EndTransmit()
			return SendCollision
		}

		nc.ctrl.EndTransmit()// so we can set Rxe to low to use the loopback to verify we transmited all the data 
	}
	
	nc.ctrl.SetTransmit(true)//We are going to use the loopback to verify we transmited all the data
	
	txByte = byte(pktType) | byte((address & 63))
	txCrc := nc.sendEscByteCrc(0, txByte, false)

	txByte = byte(nc.NodeID & 0x3F)
	txCrc = nc.sendEscByteCrc(txCrc, txByte, false)

	for i := range buffer {
		txCrc = nc.sendEscByteCrc(txCrc, buffer[i], false)
	}

    txCrc = crc16.Complete(txCrc, nc.crcTable)
    
	// Send CRC
	nc.sendEscByteCrc(0, byte(txCrc>>8), true)
	nc.sendEscByteCrc(0, byte(txCrc&0xFF), true)

	// Send the STOP byte
	nc.s.Write([]byte{stopByte})
	
	var recv = make([]byte, 1)//We are going to read the transmited data byte by byte
	tmout := time.After(time.Second * 1 )// delay of 1 second in case we stall # need to be changed to a value based on the max frame lenght and lowest allowed baudrate
	
	for {
		select {
			case <- tmout:
                log.Println("Tx timeout")
                goto TXEND
			default:
				n,_:=nc.s.Read(recv)
				if n > 0 && recv[0]==stopByte { 
					log.Println("Tx finished")
					goto TXEND
				}
		}
	}
TXEND:
	nc.ctrl.EndTransmit()
	return SendOk
}

// Send - Send a packet
func (nc *NodeController) Send(mcast bool, address int, buffer []byte, length int, retry int) SendStatus {
	// Check current RX state. If data is currently being received, we can not send data.
	if nc.rxBusy() {
		return SendRxBusy
	}

	// Add 1 to retry, as we want to send at least once.
	retry++

	// The current result. Initialized, as a warning is generated if not, even if init will always happen!
	sendResult := SendOk

	// Sends packet to specified node address
	for retry > 0 {
		// Add check for collision
		pktType := PktTypeSingle
		if mcast {
			pktType = PktTypeMulticast
		}

		sendResult = nc.sendRaw(pktType, address, buffer, length)
		retry--

		if sendResult == SendCollision {
			log.Println("TX: collission, delaying.")
			if retry != 0 {
				nc.delayRandom()
				continue
			}
		}

		if mcast {
			break
		}

		// Try to receive an ACK packet
		//ackRecvStatus := nc.recvPacket(1)
		ackRecvStatus := nc.recvPacket(100)//changed it to be the same value as RETRY_DELAY_MAX 
		log.Println("TX: ack state %d", ackRecvStatus)

		// Anything except data ready, means we need to retry. Continue...
		if ackRecvStatus != RecvDataReady {
			sendResult = SendNoAck
			continue
		}

		if nc.Packet.Type == PktTypeAck {
			break
		}

		if nc.Packet.Type < PktTypeAck || nc.Packet.State != PktStatusOk {
			return SendCollision
		}
		// The received packet was a valid NACK, we simply wrap around and retry. which means we should retry...
	}

	return sendResult
}
